extern crate xekeys;

use libtui::window::{Config, LineNum, Location, Window};
use xekeys::{get_keys, ArrowKey, Keys};

fn main() {
    const LOREM: &str = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean purus lectus, pharetra interdum felis id, suscipit pulvinar arcu.
Fusce ut eleifend orci. Vestibulum nec fringilla neque.
Nullam risus lacus, tincidunt non odio non, mattis rutrum sem.
Interdum et malesuada fames ac ante ipsum primis in faucibus.
Sed condimentum, arcu eu euismod accumsan, diam odio tristique arcu, volutpat egestas elit nunc nec erat.
Pellentesque eget quam ac dolor suscipit blandit.
Curabitur suscipit condimentum mauris, eget porta justo vulputate lobortis.
Nullam et tempor ligula.
Mauris porta pellentesque tellus quis sagittis.
Phasellus sit amet elit nec ipsum dictum laoreet luctus id ligula. Sed ac pretium tellus.
Fusce sit amet congue ex, et varius enim.
Praesent sit amet pretium augue, vitae volutpat ipsum.

Suspendisse lacinia malesuada dolor, sed pellentesque eros egestas ac.
Donec porta, tortor eget cursus commodo, sapien lacus congue magna, non lacinia ipsum nunc ac nunc.
Morbi vel dui vel mi luctus mattis nec nec nulla.
Integer a lacinia sapien. Nullam sed tortor massa.
Sed sit amet feugiat ante, non egestas neque.
Donec bibendum ultricies viverra.
Sed sit amet tincidunt magna.
Nam eget massa viverra, efficitur eros vel, tempus velit.
Aenean luctus lectus massa, nec sagittis dui congue vel.
Donec id mauris diam.
Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
Sed bibendum sem ac auctor malesuada.
Praesent auctor, augue quis pellentesque sollicitudin, erat orci porta lectus, a malesuada justo sem at erat.
Phasellus maximus justo eu lacus pharetra ullamcorper.";
    let term = libtui::terminal::Terminal::default();
    let mut app = libtui::init(true);
    app.push(Window::new("Jeff".to_owned(),
                         (term.get_size().0 / 2, term.get_size().1 / 2),
                         (0, 0),
                         LOREM.to_owned(),
                         None,
                         None,
                         [true, true, true, false]));
    app.push(Window::new("Jeff2".to_owned(), (term.get_size().0 / 2 - 2, term.get_size().1 / 2), (term.get_size().0 / 2 + 1, 0), LOREM.to_owned(), Some((1, 6)), None, [true, true, true, true]));
    app.push(Window::new("Cmds".to_owned(), (term.get_size().0, term.get_size().1 / 2), (0, term.get_size().1 / 2 + 1), ":".to_owned(), None, None, [false, false, false, true]));
    const BORDER: [&str; 4] = ["\x1b[7m ", " \x1b[0m", "\x1b[7m ", " \x1b[0m"];
    let conf = Config { title:            Location::Center,
                        border_connector: BORDER,
                        border_len:       [1, 1, 1, 1],
                        border_h:         " ",
                        border_v:         "\x1b[7m \x1b[0m",
                        line_num:         LineNum { std_num_color: "\x1b[7m", current_num_color: "\x1b[43;30m" } };
    loop {
        let keys = get_keys();

        match keys {
            | Keys::CtrlChar('q') => break,
            | Keys::Arrow(ArrowKey::Right) => {
                let app_win = app.wins[0].dimensions.clone();
                app.wins[0].set_size((app_win.width + 1, app_win.height));
            },
            | Keys::Arrow(ArrowKey::Left) => {
                let app_win = app.wins[0].dimensions.clone();
                app.wins[0].set_size((app_win.width - 1, app_win.height));
            },
            | Keys::Arrow(ArrowKey::Up) => {
                let app_win = app.wins[0].dimensions.clone();
                app.wins[0].set_size((app_win.width, app_win.height - 1));
            },
            | Keys::Arrow(ArrowKey::Down) => {
                let app_win = app.wins[0].dimensions.clone();
                app.wins[0].set_size((app_win.width, app_win.height + 1));
            },
            | _ => ()
        };
        let app_win = app.wins[0].dimensions.clone();
        app.wins[1].pos.x = app_win.width + 1;
        app.wins[1].set_size((term.get_size().0 - app_win.width - 2, app_win.height));
        app.wins[2].pos.y = app_win.height;
        app.wins[2].set_size((term.get_size().0, term.get_size().1 - app_win.height));
        app.render(&conf);
    }
}
