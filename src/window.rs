#[derive(Clone)]
pub struct Window {
    pub title:               String,
    pub dimensions:          Dimensions,
    pub pos:                 Pos,
    pub content:             String,
    pub left_side_line_num:  Option<(usize, usize)>,
    pub right_side_line_num: Option<(usize, usize)>,
    pub left_column:         bool,
    pub right_column:        bool,
    pub top_bar:             bool,
    pub bottom_bar:          bool
}

#[derive(Clone)]
pub struct Dimensions {
    pub width:  u16,
    pub height: u16
}

#[derive(Clone)]
pub struct Pos {
    pub x: u16,
    pub y: u16
}

pub struct Config<'a> {
    pub title:            Location,
    /// Order: TopRight, TopLeft, BottomRight, BottomLeft
    pub border_connector: [&'static str; 4],
    pub border_len:       [usize; 4],
    /// If a border len was [",", ".", "`", "'"] and the border_h was '-' while the border_v was
    /// '|' a box would look like:
    /// ,---.
    /// |   |
    /// `---'
    pub border_h:         &'a str,
    pub border_v:         &'a str,
    pub line_num:         LineNum<'a>
}

pub enum Location {
    Right,
    Left,
    Center
}

pub struct LineNum<'a> {
    pub std_num_color:     &'a str,
    pub current_num_color: &'a str
}

impl Window {
    /// Columns goes top, bottom, left, right.
    pub fn new(title: String, dimensions: (u16, u16), pos: (u16, u16), content: String, left_side_line_num: Option<(usize, usize)>, right_side_line_num: Option<(usize, usize)>, columns: [bool; 4])
               -> Window {
        Window { title,
                 dimensions: Dimensions { width: dimensions.0, height: dimensions.1 },
                 pos: Pos { x: pos.0, y: pos.1 },
                 content,
                 left_side_line_num,
                 right_side_line_num,
                 left_column: columns[2],
                 right_column: columns[3],
                 top_bar: columns[0],
                 bottom_bar: columns[1] }
    }

    pub fn set_size(&mut self, size: (u16, u16)) {
        self.dimensions = Dimensions { width: size.0, height: size.1 };
    }
}
