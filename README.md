# Libtui
Libtui is a graphics library for the terminal.
Libtui focuses on having a small number of crates which is why a lot is expected for the user to decide, such a line wrapping.
Libtui is also highly customizable so it can fit almost any vision.
To try it out, look at the examples, which you can test by using `cargo run --release --example main`. Use arrow keys to move the windows.
NOTE: keypresses are not handled by libtui; the examples uses a seperate crate I made to handle keypresses.
## License
Libtui uses MPL-2.0. In order to use this crate you must state that your project uses this crate and a link to the [MPL](https://mozilla.org/en-US/MPL/2.0/)
